package com.airline.companymanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeItem {
    private String name;
    private String phone;
}
