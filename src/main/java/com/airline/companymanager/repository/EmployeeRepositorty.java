package com.airline.companymanager.repository;

import com.airline.companymanager.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepositorty extends JpaRepository<Employee, Long> {
}
