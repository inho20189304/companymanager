package com.airline.companymanager.service;

import com.airline.companymanager.entity.Employee;
import com.airline.companymanager.model.EmployeeItem;
import com.airline.companymanager.repository.EmployeeRepositorty;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepositorty employeeRepositorty;

    public void setEmployee(String name, String phone, LocalDate birthday) {
        Employee addData = new Employee();
        addData.setName(name);
        addData.setPhone(phone);
        addData.setBirthday(birthday);

        employeeRepositorty.save(addData);

    }
    public List<EmployeeItem> getEmployees() {
        List<EmployeeItem> result = new LinkedList<>();

        List<Employee> originData = employeeRepositorty.findAll();

        for (Employee item : originData) {
            EmployeeItem addItem = new EmployeeItem();
            addItem.setName(item.getName());
            addItem.setPhone(item.getPhone());

            result.add(addItem);
        }
        return result;
    }
}
