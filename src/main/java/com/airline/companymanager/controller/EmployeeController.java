package com.airline.companymanager.controller;

import com.airline.companymanager.model.EmployeeItem;
import com.airline.companymanager.model.EmployeeRegistration;
import com.airline.companymanager.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.SecureRandom;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @PostMapping("/data")
    public String setEmployee(@RequestBody EmployeeRegistration registration) {
        employeeService.setEmployee(registration.getName(), registration.getPhone(), registration.getBirthday());
        return "ok";
    }
    @GetMapping("/employees")
    public List<EmployeeItem> getEmployees() {
        List<EmployeeItem> result = employeeService.getEmployees();
        return result;
    }
}
